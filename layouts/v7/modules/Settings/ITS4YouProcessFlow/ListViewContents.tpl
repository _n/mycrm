{*<!--
/*********************************************************************************
* The content of this file is subject to the Process Flow 4 You.
* ("License"); You may not use this file except in compliance with the License
* The Initial Developer of the Original Code is IT-Solutions4You s.r.o.
* Portions created by IT-Solutions4You s.r.o. are Copyright(C) IT-Solutions4You s.r.o.
* All Rights Reserved.
********************************************************************************/
-->*}
<div class="listViewEntriesDiv" style='overflow-x:auto;'>
    <div class="col-sm-12 col-xs-12 ">

        <div class="col-lg-4 col-sm-6 col-xs-8">
            <div class="table-container">
                <table class="table listview-table">
                    <thead>
                    <tr class="listViewContentHeader">
                        <th></th>
                        <th>{vtranslate("LBL_MODULE", $QUALIFIED_MODULE)}</th>
                    </tr>
                    </thead>
                    {foreach item=S_MODULE_MODEL key=TAB_ID from=$ALL_MODULES}
                        <tr class="listViewEntries" data-recordurl="index.php?module=ITS4YouProcessFlow&view=Detail&parent=Settings&sourceModule={$S_MODULE_MODEL->getName()}">
                            <td></td>
                            <td class="listViewEntryValue" nowrap>
                                {vtranslate($S_MODULE_MODEL->getName(),$S_MODULE_MODEL->getName())}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<br>
<div align="center" class="small" style="color: rgb(153, 153, 153);">{vtranslate("ITS4YouProcessFlow",$QUALIFIED_MODULE)} {$VERSION} {vtranslate("COPYRIGHT",$QUALIFIED_MODULE)}</div>
<br>