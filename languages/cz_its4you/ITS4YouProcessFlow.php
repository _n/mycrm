<?php
/* *********************************************************************************
 * The content of this file is subject to the ITS4YouProcessFlow license.
 * ("License"); You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is IT-Solutions4You s.r.o.
 * Portions created by IT-Solutions4You s.r.o. are Copyright(C) IT-Solutions4You s.r.o.
 * All Rights Reserved.
 * ******************************************************************************* */

$languageStrings = array(
	'ITS4YouProcessFlow' => 'Process Flow 4 You', 
	'LBL_PROCESSFLOW_NAME' => 'Name', 
	'LBL_TARGET_MODULE' => 'Module', 
	'LBL_PROCESSFLOW_CONDITION' => 'Conditions', 
	'COPYRIGHT' => 'IT-Solutions4You', 
	'LBL_IS_NOT' => 'is not', 
	'LBL_DEBUG' => 'Debug Record', 
);

$jsLanguageStrings = array(
); 

